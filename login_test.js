Feature('login');

Scenario('Login com sucesso',  ({ I }) => {

    I.amOnPage('https://automationpratice.com.br/');
    I.click('Login')
    I.fillField('#user',('diegodangeloferreira10@gmail.com'))
    I.fillField('#password','ferreira35')
    I.click('#btnLogin')
    I.waitForText('Login realizado',3)

}).tag('@cenario1')

Scenario('Tentando Logar escrever apenas o e-mail',  ({ I }) => {

    I.amOnPage('https://automationpratice.com.br/');
    I.click('Login')
    I.fillField('#user',('diegodangeloferreira10@gmail.com'))
    I.click('#btnLogin')
    I.waitForText('Senha inválida.',3)

}).tag('@cenario2')

Scenario('Tentando logar sem digitar e-mail e senha',  ({ I }) => {

    I.amOnPage('https://automationpratice.com.br/');
    I.click('Login')
    I.waitForText('Login',3)
    I.click('#btnLogin')
    I.waitForText('E-mail inválido',3)

}).tag('@cenario3')

Scenario('Tentando Logar escrever apenas a senha',  ({ I }) => {

    I.amOnPage('https://automationpratice.com.br/');
    I.click('Login')
    I.fillField('#password','ferreira35')
    I.click('#btnLogin')
    I.waitForText('E-mail inválido',3)

}).tag('@cenario4')



